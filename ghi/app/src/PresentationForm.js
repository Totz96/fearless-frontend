import React from 'react'

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            presenter_name: '',
            company_name: '',
            presenter_email: '',
            title: '',
            synopsis: '',
            status: "",
            conference: '',
            conferenceHref: '',
            conferences: [],
        }
        this.handleCompanyChange = this.handleCompanyChange.bind(this)
        this.handleConferenceChange = this.handleConferenceChange.bind(this)
        this.handleEmailChange = this.handleEmailChange.bind(this)
        this.handlePresenterChange = this.handlePresenterChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this)
        this.handleTitleChange = this.handleTitleChange.bind(this)
    }

    handlePresenterChange(event) {
        const value = event.target.value
        this.setState({presenter_name: value})
    }

    handleCompanyChange(event) {
        const value = event.target.value
        this.setState({company_name: value})
    }

    handleEmailChange(event) {
        const value = event.target.value
        this.setState({presenter_email: value})
    }

    handleTitleChange(event) {
        const value = event.target.value
        this.setState({title: value})
    }

    handleSynopsisChange(event) {
        const value = event.target.value
        this.setState({synopsis: value})
    }

    handleConferenceChange(event) {
        const value = event.target.value
        this.setState({conferenceHref: value})
        const hrefSplit = value.split("/")
        // console.log(hrefSplit)
        this.setState({conference: parseInt(hrefSplit[3])})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        delete data.conferences
        delete data.conferenceHref
        console.log(data)

        const presentationUrl = `http://localhost:8000${this.state.conferenceHref}presentations/`
        // console.log(locationUrl)
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(presentationUrl, fetchConfig)
        if (response.ok) {
            const newPresentation = await response.json()
            console.log(newPresentation)

            this.setState({
                presenter_name: '',
                company_name: '',
                presenter_email: '',
                title: '',
                synopsis: '',
                status: "",
                conference: '',
                conferenceHref: '',
            })
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            // console.log(data)
            this.setState({conferences: data.conferences})
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePresenterChange} value={this.state.presenter_name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmailChange} value={this.state.presenter_email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCompanyChange} value={this.state.company_name} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea onChange={this.handleSynopsisChange} value={this.state.synopsis} className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleConferenceChange} value={this.state.conferenceName} required name="conference" id="conference" className="form-select">
                                    <option value="">Choose a conference</option>
                                    {this.state.conferences.map(conference => {
                                        return (
                                            <option key={conference.href} value={conference.href}>
                                                {conference.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default PresentationForm