// function to create the html for a conference card
function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="col">
            <div class="card" style="box-shadow: 5px 5px 3px grey">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class-"card-subtitle mb-3" style="color: grey; margin-bottom: 16px">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${starts} - ${ends}
                </div>
            </div>
        </div>
    `
}



function makeAlert(message, type) {
    let alertPlaceholder = document.getElementById('liveAlertPlaceholder')
    let wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    alertPlaceholder.append(wrapper)
}


window.addEventListener('DOMContentLoaded', async () => {
    

    const url = 'http://localhost:8000/api/conferences/'
    
    try {
        // requests data from the api with the api url from above
        const response = await fetch(url)
        
        // if the response is not okay
        if (!response.ok) {
            // figure out what to do if the response is bad
            // throw new Error('conference list response not ok')
            makeAlert('We are having difficulty getting the conferences.', 'warning')
            
        } else { // if the response is valid
            // get the json response data
            const data = await response.json()

            for (let conference of data.conferences) {
                // getting the conference detail data from the api
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)
                if (detailResponse.ok) {
                    // if response ok get the conference details from the detail data
                    const details = await detailResponse.json()
                    const title = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const location = details.conference.location.name
                    const startsDate = new Date(details.conference.starts)
                    const starts = startsDate.toLocaleDateString()
                    const endsDate = new Date(details.conference.ends)
                    const ends = endsDate.toLocaleDateString()
                    // load the detail data into the html string function createCard
                    const html = createCard(title, description, pictureUrl, starts, ends, location)
                    const column = document.querySelector('.row')
                    column.innerHTML += html
                    
                } else {
                    // throw an error if the detailResponse is bad
                    // throw new Error('conference detail response not ok')
                    makeAlert('We are having difficulty getting the conference details.', 'warning')

                }

            }

        }
    } catch (error) {
        // figure out what to do if an error is raised
        console.error('error', error)
    }


});
