window.addEventListener('DOMContentLoaded', async () => {
    // console.log("javascript loaded")
    const selectTag = document.getElementById('conference');
    
    // getting the list of conferences from the api
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        // showing the conferences in a dropdown
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        // removed the d-none from the selectTag class unhiding it
        selectTag.classList.remove("d-none")

        // adds the d-none to the divTag hiding the loading spinner
        const divTag = document.getElementById('loading-conference-spinner')
        divTag.classList.add("d-none")

        // sending the data from the form to the api
        const formTag = document.getElementById('create-attendee-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))
            // console.log(json)
            const attendeeUrl = 'http://localhost:8001/api/attendees/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            const responsePost = await fetch(attendeeUrl, fetchConfig)
            if (responsePost.ok) {
                // hide the form and unhide the success alert
                formTag.classList.add("d-none")
                const alertTag = document.getElementById("success-message")
                alertTag.classList.remove("d-none")
                
            } else {
                console.log("the post response was bad")
            }


        })

    } else {
        console.log("the get response was bad")
    }
  
});