window.addEventListener('DOMContentLoaded', async () => {
    // console.log('checkStatus.js loaded')
                            // cookieStore does not work with all browsers consider changing to document.cookie
    const payloadCookie = await cookieStore.get('jwt_access_payload')
    if (payloadCookie) {
            // console.log(payloadCookie)
        // The cookie value is a JSON-formatted string, so parse it
        const encodedPayload = JSON.parse(payloadCookie.value);

        // Convert the encoded payload from base64 to normal string
        const decodedPayload = atob(encodedPayload)

        // The payload is a JSON-formatted string, so parse it
        const payload = JSON.parse(decodedPayload)

        // Print the payload
        // console.log(payload)

        const perms = payload.user.perms
        // console.log(perms)

        if (perms.includes("events.add_location")) {
            const newLocationLinkTag = document.getElementById('new-location')
            newLocationLinkTag.classList.remove("d-none")
        }

        if (perms.includes("events.add_conference")) {
            const newConferenceLinkTag = document.getElementById('new-conference')
            newConferenceLinkTag.classList.remove("d-none")
        }

        if (perms.includes("presentations.add_presentation")) {
            const newPresentationLinkTag = document.getElementById('new-presentation')
            newPresentationLinkTag.classList.remove("d-none")
        }
    }
})
