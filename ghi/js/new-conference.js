window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/'
    const response = await fetch(url)

    if (!response.ok){
        // handle bad response
        console.log("the get response was bad")
    } else {
        // load data and get array of locations
        const data = await response.json()
        // console.log(data)
        const locations = data.locations
        // selects the select tag with the id of 'location' to add options to later
        let selectTag = document.getElementById('location')

        // creates the form options for each location
        for (let location of locations) {
            let option = document.createElement('option')
            option.value = location['id']
            option.innerHTML = location['name']
            selectTag.appendChild(option)
        }

        // adds an eventListener to the submit button for sending data to the api
        const formTag = document.getElementById('create-conference-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            // creating the json data
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))
            // console.log(json)

            // sending the json data to the api
            const conferenceUrl = 'http://localhost:8000/api/conferences/'
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            }
            const responsePost = await fetch(conferenceUrl, fetchConfig)
            if (responsePost.ok) {
                formTag.reset()
                // const newConference = await responsePost.json()
                // console.log(newConference)
            } else {
                console.log("the post response was bad")
            }

        })

    }
})