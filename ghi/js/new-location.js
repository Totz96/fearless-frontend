window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/'

        const response = await fetch(url)

        if (!response.ok) {
            // handle bad response
            console.log("the get response was bad")
        } else {
            // load data and get the array of states
            const data = await response.json()
            // console.log(data)
            const states = data.states
            let selectTag = document.getElementById('state')
    
            // creates form options for each of the states
            for (let state of states) {
                let option = document.createElement("option")
                option.value = state['abbreviation']
                option.innerHTML = state['name']
                selectTag.appendChild(option)
            }

            // adds an eventListener to the submit button to send the data to the api on submit
            const formTag = document.getElementById('create-location-form')
            formTag.addEventListener('submit', async event => {
                event.preventDefault()
                // making a formData object and converting to json
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData))
                
                // sending the json data to create the location
                const locationUrl = 'http://localhost:8000/api/locations/'
                const fetchConfig = {
                  method: "post",
                  body: json,
                  headers: {
                    'Content-Type': 'application/json',
                  },
                }
                const responsePost = await fetch(locationUrl, fetchConfig)
                if (responsePost.ok) {
                    // resets the form
                  formTag.reset()
                //   const newLocation = await responsePost.json()
                //   console.log(newLocation)
                } else {
                    console.log("the post response was bad")
                }
            })
        }
    })

            
            
            



